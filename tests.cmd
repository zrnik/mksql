@echo off

:: All tests:
vendor\bin\phpunit

:: Unit Tests
:: vendor\bin\phpunit --testsuite unit

:: Integration Tests
:: vendor\bin\phpunit --testsuite integration
